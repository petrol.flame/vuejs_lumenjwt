# JWT REST_api Laravel/Lumen

## Project setup

```
composer install

```

  

### Copy .env File

```
cp .env.example .env

```

### Set DB and username in .env

```
DB_DATABASE= <your db name >
DB_USERNAME= <username example: root>
DB_PASSWORD= <password here leave empty if none>

```

### Run migrations 

```
php artisan migrate

```

  

### Set the JWTAuth secret key used to sign the tokens 

```
php artisan jwt:secret

```

  

### Start localhost server for Lumen

```
php -S localhost:3000 -t public

```
