<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (User::get()->count() < 1) {
            User::create(['name' => 'jack', 'email' => 'jack@vue.com', 'password' => Hash::make('123456')]);
            User::factory()->count(9)->create();
        }
    }
}
