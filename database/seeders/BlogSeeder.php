<?php

namespace Database\Seeders;

use App\Models\Blog;
use Exception;
use Illuminate\Database\Seeder;

class BlogSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $qty = (int) $this->command->ask("Enter number of seeds for BlogSeeder", 0);
        if($qty > 0){
            Blog::truncate();
            if(Blog::get()->count() < 1){
                Blog::factory()->count($qty)->create();
            }
        }else{
            throw new Exception('error intger required and must be greater than 0');
        }
    }
}
