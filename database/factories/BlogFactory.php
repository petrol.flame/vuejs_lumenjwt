<?php

namespace Database\Factories;

use App\Models\Blog;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class BlogFactory extends Factory
{
    protected $model = Blog::class;

    public function definition(): array
    {
    	return [
            'user_id' => User::all()->random()->id,
            'title' => $this->faker->unique()->safeEmail,
            'blog' => $this->faker->paragraph(),
    	];
    }
}
