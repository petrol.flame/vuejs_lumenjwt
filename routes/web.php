<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return response()->json(['message'=>"lumen/laravel"],200);
});

$router->post('register', 'UserController@registerUser');
$router->post('login', 'UserController@loginUser');

$router->group(['middleware' => 'auth'],function() use($router){

    $router->get('authuser','authController@authorizedUser');

    $router->post('create-blog','BlogController@create');
    $router->get('fetch-blogs','BlogController@index');
    $router->get('list-blogs','BlogController@list');
    $router->get('fetch-blog-of-user/{id}','BlogController@show_perUser');
    $router->delete('delete-blog/{id}','BlogController@destroy');

    $router->get('logout','authController@logout');
    $router->get('get-blog/{id}','BlogController@view');

});

