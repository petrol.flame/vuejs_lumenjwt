<?php

use Illuminate\Support\Facades\Artisan;
use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

class UserAuthTest extends TestCase
{
    /**
     * @test
     */
    public function CheckAuth()
    {
        Artisan::call('db:seed --class=UserSeeder');

        $credentials = ['name' => 'jack', 'email' => 'jack@vue.com', 'password' => '123456'];

        $response = $this->json('POST','login',$credentials);

        $response->assertResponseOk();
        //Extracting the json from the Request
        $Getjson = $response->response->getContent();

        //Decode the String Json Object
        $res_json = json_decode($Getjson);

        $token = $res_json->token;

        $response2 = $this->get('authuser',['HTTP_Authorization' => 'Bearer '.$token])->seeJsonStructure([
            'msg',
            'data' => [
                'id',
                'name',
                'email',
            ]
        ]);

        $response2->assertResponseOk();
    }
}
