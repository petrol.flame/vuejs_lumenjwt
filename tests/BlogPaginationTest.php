<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

class BlogPaginationTest extends TestCase
{
    protected $user;
    protected $json;

    public function default()
    {
        $this->user = $this->getloginUser();
        $this->user->assertResponseOk();
        $this->json = json_decode($this->user->response->getContent());
    }
    /**
     * @test
    */
    public function test_BlogPagination()
    {
        $this->default();
        $json = $this->json;
        $token = $json->token;
        $listing = $this->get('list-blogs',['HTTP_Authorization' => 'Bearer '.$token]);
        $listing->assertResponseOk();
    }
}
