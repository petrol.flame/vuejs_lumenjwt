<?php

use App\Models\Blog;
use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

class BlogModuleTest extends TestCase
{
    protected $user;
    protected $json;

    public function default()
    {
        $this->user = $this->getloginUser();
        $this->user->assertResponseOk();
        $this->json = json_decode($this->user->response->getContent());
    }

    /**
     * @test
    */
    public function CreateBlogValdiationErrors()
    {
        $this->default();
        $json = $this->json;
        $token = $json->token;
        $response = $this->post('create-blog',[],['HTTP_Authorization' => 'Bearer ' . $token]);
        $response->assertResponseStatus(422);
        $response->seeJsonStructure([
            'msg' => [
                'user_id',
                'title',
                'blog',
            ]
        ]);
    }

    /**
     * @test
     */
    public function CreateBlog()
    {
        $this->default();
        $json = $this->json;
        $token = $json->token;
        $data = [
            'user_id' => $json->data->id,
            'title' => $this->faker->title,
            'blog' => $this->faker->paragraph,
        ];
        $response = $this->json('POST', 'create-blog', $data, ['HTTP_Authorization' => 'Bearer ' . $token]);
        $response->assertResponseOk();
        $response->seeJsonStructure([
            'msg',
            'data' => [
                'user_id',
                'title',
                'blog',
            ]
        ]);
    }

    /**
     * @test
     */
     public function FetchBlogs()
     {
        $this->default();
        Blog::factory()->count(5)->create();
        $token = $this->json->token;
        $response = $this->get('fetch-blogs',['HTTP_Authorization' => 'Bearer '.$token]);
        $response->assertResponseOk();
        $response->seeJsonStructure([
            'msg',
            'data' =>[
                '*' => [
                    'id',
                    'user_id',
                    'title',
                    'blog',
                ]
            ]
        ]);
    }

    /**
     * @test
    */
    public function FetchBlogPerUser()
    {
        $this->default();
        Blog::factory()->count(15)->create();
        $token = $this->json->token;
        $response = $this->get('fetch-blog-of-user/1',['HTTP_Authorization' => 'Bearer '.$token]);
        $response->assertResponseOk();
    }
}
