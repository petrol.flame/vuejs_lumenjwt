<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

class BaseTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @test
     */
    public function BasicTest()
    {
        $this->get('/');

        $this->assertResponseStatus(200);
    }
}
