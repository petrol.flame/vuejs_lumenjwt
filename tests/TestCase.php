<?php

use Faker\Factory as Faker;
use Illuminate\Support\Facades\Artisan;
use Laravel\Lumen\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    protected $faker;
    /**
     * Creates the application.
     *
     * @return \Laravel\Lumen\Application
     */
    public function createApplication()
    {
        return require __DIR__.'/../bootstrap/app.php';
    }
    /**
     * Sets up the tests
     */
    public function setUp(): void
    {
        parent::setUp();

        $this->faker = Faker::create();

        Artisan::call('migrate'); // runs the migration
    }

    /**
     * Rolls back migrations
     */
    public function tearDown(): void
    {
        Artisan::call('migrate:rollback');

        parent::tearDown();
    }

    public function getloginUser()
    {
        Artisan::call('db:seed --class=UserSeeder');

        $credentials = ['name' => 'jack', 'email' => 'jack@vue.com', 'password' => '123456'];

        $response = $this->json('POST','login',$credentials);

        return $response;
    }
}
