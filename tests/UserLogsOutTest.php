<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

class UserLogsOutTest extends TestCase
{
    /**
     * @test
     */
    public function UserLogout()
    {
        $response = $this->getloginUser();

        $response->assertResponseOk();

        $data = json_decode($response->response->getContent());

        $bearer_token = $data->token;

        $response2 = $this->get('authuser',['HTTP_Authorization' => 'Bearer '.$bearer_token]);

        $response2->assertResponseOk();
    }
}
