<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

class UserRegistersTest extends TestCase
{
    /**
     *
     * @test
     */
    public function UserRegisters()
    {
        $password = $this->faker->word();

        $data = [
            'name' => $this->faker->name,
            'email' => $this->faker->email,
            'password' => $password,
            'password_confirmation' => $password,
        ];

        $response = $this->json("POST",'register',$data)->seeJsonStructure([
            'data' => [
                'name',
                'email',
                'id',
            ],
            'token',
            'token_type',
        ]);

        //Extracting the json from the Request
        // $Getjson = $response->response->getContent();

        //Decode the String Json Object
        // $res_json = json_decode($Getjson);

        $response->assertResponseOk();
    }
}
