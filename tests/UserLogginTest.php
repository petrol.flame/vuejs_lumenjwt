<?php

use Illuminate\Support\Facades\Artisan;
use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

class UserLogginTest extends TestCase
{
    /**
     * @test
     */
    public function UserLogsIn()
    {
        Artisan::call('db:seed --class=UserSeeder');

        $credentials = ['name' => 'jack', 'email' => 'jack@vue.com', 'password' => '123456'];

        $response = $this->json('POST','login',$credentials)->seeJsonStructure([
            'data' => [
                'name',
                'email',
                'id',
            ],
            'token',
            'token_type',
        ]);

        $response->assertResponseOk();
        //Extracting the json from the Request
        $Getjson = $response->response->getContent();

        //Decode the String Json Object
        $res_json = json_decode($Getjson);

        $this->token = $res_json->token;
    }

}
