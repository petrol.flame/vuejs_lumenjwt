<?php

namespace App\Http\Controllers;

use App\Models\Blog;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class BlogController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $data = Blog::all();
        return $this->send_JsontoClient('all blogs', 200, $data);
    }

    public function create(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'user_id' => 'required|numeric|exists:users,id',
            'title' => 'required|string',
            'blog' => 'required|string',
        ]);
        if ($validation->fails()) {
            return $this->send_JsontoClient($validation->errors(), 422);
        } else {
            try {
                $data = Blog::create($request->all());
                return $this->send_JsontoClient('blog created', 200, $data);
            } catch (Exception $e) {
                return response($e->getMessage(), 400);
            }
        }
    }


    public function view($id)
    {
        try {
            $data=Blog::find($id);

            if(!$data)
            {
                return $this->send_JsontoClient("not found",404,null);
            }
            return $this->send_JsontoClient("success",200,$data);

        } catch (\Exception $e)  {
            throw $e;

        }
    }

    public function show_perUser(Request $request, $id)
    {
        try {
            $page = Blog::where('user_id', $id)->orderby('id','DESC')->paginate(5);
            $data = $this->custom_pagination($page);
            return $this->send_JsontoClient('records fetched', 200, $data);
        } catch (Exception $e) {
            return response($e->getMessage(), 400);
        }
    }

    public function destroy($id)
    {
        $data = Blog::find($id);
        if($data){
            $data->delete();
            return $this->send_JsontoClient('blog deleted successfuly',200);
        }
        return $this->send_JsontoClient('record not found',404);
    }

    public function list(Request $request)
    {
        try {
            $page = Blog::orderby('id', 'DESC')->paginate(5);
            $data = $this->custom_pagination($page);
            return $this->send_JsontoClient('records fetched', 200, $data);
        } catch (Exception $e) {
            return response()->json($e->getMessage());
        }
    }

    private function custom_pagination($pagination)
    {
        $links = [];
        $current = $pagination->currentPage();
        $data = $pagination->toArray();
        $path = $data['path'];
        $next = $data['links'][count($data['links']) - 1];
        $previous = $data['links'][0];
        $limit = $data['last_page'];
        for ($i = 0; $i < $limit + 2; $i++) {
            if ($i == 0) {
                array_push($links, $previous);
            } else if ($i > 0 && $i < $limit + 1) {
                $set = [
                    'url' => $path . '?page=' . $i,
                    'label' => strval($i),
                    'active' => false,
                ];
                array_push($links, $set);
            } else {
                array_push($links, $next);
            }
        }
        $links[$current]['active'] = true;
        $data['links'] = $links;
        return $data;
    }
}
